import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import axios, { AxiosError } from 'axios';
import { ENDPOINT_MS_PROJECTS } from 'react-native-dotenv';

interface Project {
  id: number;
  name: string;
}

const ProjectManagementScreen = ({ navigation }: any) => {
  const [projects, setProjects] = useState<Project[]>([]);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchProjects = async () => {
      try {
        const response = await axios.get<Project[]>(`${ENDPOINT_MS_PROJECTS}/projects`);
        setProjects(response.data);
      } catch (error: any) {
        console.error('Error fetching projects:', error);

        if (axios.isAxiosError(error)) {
          setError(error.response?.data?.message || 'Error desconocido al obtener proyectos');
        } else {
          setError('Error desconocido al obtener proyectos');
        }
      }
    };

    fetchProjects();
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Project Management</Text>
      {projects.map((project) => (
        <Text key={project.id}>{project.name}</Text>
      ))}
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('UserManagement')}>
          <Text style={styles.buttonText}>Usuarios</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('TeamManagement')}>
          <Text style={styles.buttonText}>Equipos</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('TaskManagement')}>
          <Text style={styles.buttonText}>Tareas</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Home')}>
          <Text style={styles.buttonText}>Volver</Text>
        </TouchableOpacity>
      </View>

      {error && (
        <Text style={styles.errorText}>{error}</Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    marginBottom: 20,
  },
  buttonContainer: {
    marginTop: 20,
  },
  button: {
    backgroundColor: '#3498db',
    padding: 10,
    borderRadius: 5,
    marginBottom: 10,
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
  },
  errorText: {
    color: 'red',
    marginTop: 10,
  },
});

export default ProjectManagementScreen;
