import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import axios from 'axios';
import { ENDPOINT_MS_USER } from 'react-native-dotenv';
import { useUserStore } from '../stores/useUserStore';

// Importa la fuente Montserrat
import { useFonts, Montserrat_700Bold } from '@expo-google-fonts/montserrat';

export function logout() {
  
}

const SignInScreen = ({ navigation }: any) => {
  const [email, setEmail] = useState('test@test.cl');
  const [password, setPassword] = useState('123456789');
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const { accessToken, setAccessToken } = useUserStore();

  // Carga la fuente Montserrat
  const [fontsLoaded] = useFonts({
    MontserratBold: Montserrat_700Bold,
  });

  const signInRequest = async (email: string, password: string) => {
    setError(false);
    console.log(`${ENDPOINT_MS_USER}/auth/login`);
    try {
      const response = await axios.post(`${ENDPOINT_MS_USER}/auth/login`, {
        email,
        password,
      });
  
      const accessToken = response?.data?.accessToken || '';
      setAccessToken(accessToken);
  
      console.log('Navigating to Home screen');
      navigation.navigate('Home');
      console.log('Navigating to Home screen');

    } catch (e: any) {
      setError(true);
      setErrorMessage(e?.response?.data?.message || 'Error desconocido');
      console.log({ error: e?.response?.data?.message, fullError: e });
    }
  };

  const resetPassword = () => {
    // Lógica para restablecer la contraseña
    // Puedes mostrar un modal, navegar a una nueva pantalla, etc.
    // Por ejemplo, puedes llamar a una función de Axios para solicitar un restablecimiento de contraseña.
  };

  if (!fontsLoaded) {
    return null; // Muestra algo mientras se cargan las fuentes
  }

  return (
    <View style={styles.container}>
      {/* Nuevo Text fijo */}
      <Text style={styles.appName}>HIRA</Text>
      <Text>Email</Text>
      <TextInput
        style={styles.input}
        placeholder="email@email.cl"
        value={email}
        inputMode="email"
        onChangeText={(text: string) => setEmail(text)}
      />
      <Text>Password</Text>
      <TextInput
        style={styles.input}
        placeholder="Password"
        value={password}
        inputMode="text"
        secureTextEntry
        onChangeText={(text: string) => setPassword(text)}
      />
      <TouchableOpacity style={styles.signInButton} onPress={() => signInRequest(email, password)}>
        <Text style={styles.signInButtonText}>Sign In</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.resetPasswordButton} onPress={() => resetPassword()}>
        <Text style={styles.resetPasswordButtonText}>Reset Password</Text>
      </TouchableOpacity>
      {accessToken && <Text>{accessToken}</Text>}
      {error && (
        <Text style={styles.errorText}>
          {errorMessage}
        </Text>
      )}
      <TouchableOpacity style={styles.homeButton} onPress={() => navigation.navigate('Home')}>
        <Text style={styles.homeButtonText}>Home</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  appName: {
    fontSize: 48, // Ajusta el tamaño de fuente a tu preferencia
    marginBottom: 20,
    fontFamily: 'MontserratBold', // Usa la fuente Montserrat
  },
  input: {
    width: '80%',
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 10,
    padding: 10,
  },
  signInButton: {
    backgroundColor: '#3498db',
    padding: 10,
    borderRadius: 5,
    marginBottom: 10,
  },
  signInButtonText: {
    color: '#fff',
    textAlign: 'center',
  },
  resetPasswordButton: {
    backgroundColor: '#e74c3c',
    padding: 10,
    borderRadius: 5,
    marginBottom: 10,
  },
  resetPasswordButtonText: {
    color: '#fff',
    textAlign: 'center',
  },
  homeButton: {
    backgroundColor: '#2ecc71',
    padding: 10,
    borderRadius: 5,
  },
  homeButtonText: {
    color: '#fff',
    textAlign: 'center',
  },
  errorText: {
    color: '#ff0000',
    fontWeight: '700',
    marginBottom: 10,
  },
});

export default SignInScreen;
