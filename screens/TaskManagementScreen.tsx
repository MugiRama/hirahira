import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import axios from 'axios';
import { ENDPOINT_MS_TASKS } from 'react-native-dotenv';

interface Task {
  id: number;
  name: string;
}

const TaskManagementScreen = ({ navigation }: any) => {
  const [tasks, setTasks] = useState<Task[]>([]);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchTasks = async () => {
      try {
        const response = await axios.get<Task[]>(`${ENDPOINT_MS_TASKS}/tasks`);
        setTasks(response.data);
      } catch (error: any) {
        console.error('Error fetching tasks:', error);

        if (axios.isAxiosError(error)) {
          setError(error.response?.data?.message || 'Error desconocido al obtener tareas');
        } else {
          setError('Error desconocido al obtener tareas');
        }
      }
    };

    fetchTasks();
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Task Management</Text>
      {tasks.map((task) => (
        <Text key={task.id}>{task.name}</Text>
      ))}
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('UserManagement')}>
          <Text style={styles.buttonText}>Usuarios</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('TeamManagement')}>
          <Text style={styles.buttonText}>Equipos</Text>
        </TouchableOpacity>
        {/* Nuevo botón para agregar comentario */}
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('AddComment')}>
          <Text style={styles.buttonText}>Agregar Comentario</Text>
        </TouchableOpacity>
        {/* Intercambié el orden de los botones */}
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Home')}>
          <Text style={styles.buttonText}>Volver</Text>
        </TouchableOpacity>
      </View>

      {error && (
        <Text style={styles.errorText}>{error}</Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    marginBottom: 20,
  },
  buttonContainer: {
    marginTop: 20,
  },
  button: {
    backgroundColor: '#3498db',
    padding: 10,
    borderRadius: 5,
    marginBottom: 10,
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
  },
  errorText: {
    color: 'red',
    marginTop: 10,
  },
});

export default TaskManagementScreen;
