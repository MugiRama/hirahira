import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import axios from 'axios';
import { ENDPOINT_MS_USER } from 'react-native-dotenv';

interface User {
  id: number;
  username: string;
}

const UserManagementScreen = ({ navigation }: any) => {
  const [users, setUsers] = useState<User[]>([]);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const response = await axios.get<User[]>(`${ENDPOINT_MS_USER}/users`);
        setUsers(response.data);
      } catch (error: any) {
        console.error('Error fetching users:', error);

        if (axios.isAxiosError(error)) {
          setError(error.response?.data?.message || 'Error desconocido al obtener usuarios');
        } else {
          setError('Error desconocido al obtener usuarios');
        }
      }
    };

    fetchUsers();
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>User Management</Text>
      <Text style={styles.subtitle}>Que deseas hacer?, elige tu opción !</Text>
      {users.map((user) => (
        <Text key={user.id}>{user.username}</Text>
      ))}
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('TeamManagement')}>
          <Text style={styles.buttonText}>Equipos</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('ProjectManagement')}>
          <Text style={styles.buttonText}>Proyectos</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('TaskManagement')}>
          <Text style={styles.buttonText}>Tareas</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Home')}>
          <Text style={styles.buttonText}>Volver</Text>
        </TouchableOpacity>
      </View>

      {error && <Text style={styles.errorText}>{error}</Text>}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    marginBottom: 10,
    fontWeight: 'bold',
  },
  subtitle: {
    fontSize: 14,
    marginBottom: 20,
    color: '#555', // Puedes ajustar el color del texto
  },
  buttonContainer: {
    marginTop: 20,
    width: '80%', // Ajusta el ancho del contenedor según tus necesidades
  },
  button: {
    backgroundColor: '#3498db',
    padding: 10,
    borderRadius: 5,
    marginBottom: 10,
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
  },
  errorText: {
    color: 'red',
    marginTop: 10,
  },
});

export default UserManagementScreen;
