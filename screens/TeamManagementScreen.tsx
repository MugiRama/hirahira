import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import axios, { AxiosError } from 'axios';
import { ENDPOINT_MS_TEAMS } from 'react-native-dotenv';

interface Team {
  id: number;
  name: string;
}

const TeamManagementScreen = ({ navigation }: any) => {
  const [teams, setTeams] = useState<Team[]>([]);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchTeams = async () => {
      try {
        const response = await axios.get<Team[]>(`${ENDPOINT_MS_TEAMS}/teams`);
        setTeams(response.data);
      } catch (error: any) {
        console.error('Error fetching teams:', error);

        if (axios.isAxiosError(error)) {
          setError(error.response?.data?.message || 'Error desconocido al obtener equipos');
        } else {
          setError('Error desconocido al obtener equipos');
        }
      }
    };

    fetchTeams();
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Team Management</Text>
      {teams.map((team) => (
        <Text key={team.id}>{team.name}</Text>
      ))}
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('UserManagement')}>
          <Text style={styles.buttonText}>Usuarios</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('ProjectManagement')}>
          <Text style={styles.buttonText}>Proyectos</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('TaskManagement')}>
          <Text style={styles.buttonText}>Tareas</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Home')}>
          <Text style={styles.buttonText}>Volver</Text>
        </TouchableOpacity>
      </View>

      {error && (
        <Text style={styles.errorText}>{error}</Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    marginBottom: 20,
  },
  buttonContainer: {
    marginTop: 20,
  },
  button: {
    backgroundColor: '#3498db',
    padding: 10,
    borderRadius: 5,
    marginBottom: 10,
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
  },
  errorText: {
    color: 'red',
    marginTop: 10,
  },
});

export default TeamManagementScreen;
