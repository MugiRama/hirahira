import { NavigationContainer } from '@react-navigation/native';
// Cambia la importación de createNativeStackNavigator
// desde 'react-native-screens/native-stack' a '@react-navigation/native-stack'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import SignInScreen from './screens/SignInScreen';
import HomeScreen from './screens/HomeScreen';
import UserManagementScreen from './screens/UserManagementScreen';
import TeamManagementScreen from './screens/TeamManagementScreen';
import ProjectManagementScreen from './screens/ProjectManagementScreen';
import TaskManagementScreen from './screens/TaskManagementScreen';

// Cambia el nombre de la importación
const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="SignIn">
        <Stack.Screen name="SignIn" component={SignInScreen} />
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{ headerShown: false }}
        />
        <Stack.Screen name="UserManagement" component={UserManagementScreen} />
        <Stack.Screen name="TeamManagement" component={TeamManagementScreen} />
        <Stack.Screen
          name="ProjectManagement"
          component={ProjectManagementScreen}
        />
        <Stack.Screen name="TaskManagement" component={TaskManagementScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
