declare module 'react-native-dotenv' {
  export const ENDPOINT_MS_USER: string;
  export const ENDPOINT_MS_TEAMS: string;
  export const ENDPOINT_MS_PROJECTS: string;
  export const ENDPOINT_MS_TASKS: string;
}
